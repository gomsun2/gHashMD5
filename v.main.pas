unit v.main;

interface

uses
  mvw.vForm,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Actions, Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls,
  Vcl.ActnMan, RzStatus, Vcl.ExtCtrls, RzPanel, Vcl.Grids, Vcl.ValEdit;

type
  TvMain = class(TvForm)
    RzVersionInfo1: TRzVersionInfo;
    ListInfo: TValueListEditor;
    Panel1: TPanel;
    LabelFile: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure ListInfoEditButtonClick(Sender: TObject);
  private
  protected
    procedure DoDropFile(const ACnt: Integer; const AFiles: TStringList); override;
  public
    { Public declarations }
  end;

var
  vMain: TvMain;

implementation

{$R *.dfm}

uses
  IOUtils, Hash, Clipbrd, UITypes
  ;

procedure TvMain.DoDropFile(const ACnt: Integer; const AFiles: TStringList);
const
  SCopyContents = 'Clicking each row''s button for copy the contents.';
var
  LFile: string;
  LBuf: TFileStream;
begin
  if AFiles.Count = 0 then
    Exit;

  LFile := AFiles[0];
  if not TFile.Exists(LFile) then
    Exit;

  LBuf := TFileStream.Create(LFile, fmOpenRead);
  try
    ListInfo.Values['Path'] := TDirectory.GetParent(LFile);
    ListInfo.Values['FileName'] := TPath.GetFileName(LFile);
    ListInfo.Values['Size'] := LBuf.Size.ToString;
    ListInfo.Values['MD5'] := THashMD5.GetHashString(LBuf);
    LabelFile.Caption := SCopyContents;//Format('Size: %d'#13#10'Name: %s' +, [LBuf.Size, LFile]);
  finally
    FreeAndNil(LBuf);
  end;
end;

procedure TvMain.FormCreate(Sender: TObject);
var
  r: Integer;
begin
  EnableDropdownFiles := True;
  ListInfo.TitleCaptions[1] := RzVersionInfo1.FileVersion;
  for r := 0 to ListInfo.Strings.Count - 1 do
  begin
    ListInfo.ItemProps[r].EditStyle := esEllipsis;
    ListInfo.ItemProps[r].ReadOnly := True;
  end;
end;

procedure TvMain.ListInfoEditButtonClick(Sender: TObject);
var
  r: Integer;
begin
  r := ListInfo.Row;
  if not ListInfo.Cells[1, r].IsEmpty then
  begin
    Clipboard.AsText := ListInfo.Cells[1, r];
    MessageDlg('Copied the "'+ ListInfo.Cells[0, r] +'" to the clipboard', mtInformation, [mbOK], 0);
  end;
end;

end.
